function getFirstHobbie(arrayOfObjects){
    let firstHobbieOfAllIndividuals=[];
    for(let index=0;index<arrayOfObjects.length;index++){
        if(arrayOfObjects[index]["hobbies"].length!=0){
            firstHobbieOfAllIndividuals.push(arrayOfObjects[index]["hobbies"][0]);
        }
    }
    return firstHobbieOfAllIndividuals;
 }
 module.exports=getFirstHobbie;