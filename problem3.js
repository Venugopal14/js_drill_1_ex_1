function findingAustralianStudent(arrayOfObjects){
    let studentsOfAustralia=[];
    for(let index=0;index<arrayOfObjects.length;index++){
        if(arrayOfObjects[index]["isStudent"]==true && arrayOfObjects[index]["country"]=="Australia"){
            studentsOfAustralia.push(arrayOfObjects[index]["name"]);
        }
    }
    return studentsOfAustralia;
 }
 module.exports=findingAustralianStudent;