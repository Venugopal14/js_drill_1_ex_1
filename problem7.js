function getNameAndEmail(arrayOfObjects,age){
    let nameEmailsOfAge25=[];
    for(let index=0;index<arrayOfObjects.length;index++){
        if(arrayOfObjects[index]["age"]==age){
            let subValues=[];
            subValues.push(arrayOfObjects[index]["name"]);
            subValues.push(arrayOfObjects[index]["email"]);
            nameEmailsOfAge25.push(subValues);
        }
    }
    return nameEmailsOfAge25;
 }
 module.exports=getNameAndEmail;