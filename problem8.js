function getCityAndCountry(arrayOfObjects){
    let cityAndCountryOfAllIndividuals=[];
    for(let index=0;index<arrayOfObjects.length;index++){
       let subsets=[];
       subsets.push(arrayOfObjects[index]["city"]);
       subsets.push(arrayOfObjects[index]["country"]);
       cityAndCountryOfAllIndividuals.push(subsets);
    }
    return cityAndCountryOfAllIndividuals;
 }
 module.exports=getCityAndCountry;