function findingHobbiesOfSpecificAge(arrayOfObjects,age){
    let hobbiesOfAge30=[];
    for(let index=0;index<arrayOfObjects.length;index++){
        if(arrayOfObjects[index]["age"]==age){
           hobbiesOfAge30.push(arrayOfObjects[index]["hobbies"]);
        }
    }
    return hobbiesOfAge30;
 }
 module.exports=findingHobbiesOfSpecificAge;